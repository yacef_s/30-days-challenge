# 30 Days Challenge Oral

## Consignes

### Embark on a 30-day challenge!

## Expectations and rules:

    Explain why you chose this challenge and what your expectations are.
    Find a video or article that inspired you.
    Make a set of rules that you decided on for the next 30 days.
Explain and justify

## Final Version

### PART 1: expectations and rules
    Explain why you chose this challenge and what your expectations are.
    Find a video or article that inspired you.
    Make a set of rules that you decided on for the next 30 days.

### Part 2: Journal
    entries in your journal in which you will explain how you feel but also the difficulties that you might face.

### Part 3: Results
    Comparing the beginning and the end of your challenge, explain the differences you notice. You will also explain if this challenge met your expectations.

### Part 4: 10-question quiz

    For this part, you have to create a quiz with 0 questions on the subject of your challenge.


## Resources:

    Try something new for 30 days - Matt Cutts, Link: https://www.youtube.com/watch?v=UNP03fDSj1U

